# crinfarr.github.io

**a website showcasing *literally nothing***

* All css/scss is in [the /assets root.](../assets/)
* All java is in [/assets/js.](../assets/js)
* All images are in [/assets/images](../assets/images)
* All other embedded files are in [/assets/embedded](../assets/embedded)